#include "hltinterface/ContainerFactory.h"
#include <stdexcept>
std::shared_ptr<hltinterface::ContainerFactory> hltinterface::ContainerFactory::m_instance(0);
bool hltinterface::ContainerFactory::m_modificationEnabled(true);

std::shared_ptr<hltinterface::ContainerFactory> hltinterface::ContainerFactory::getInstance(){return m_instance;}

// prevent anybody from resetting the factory
bool hltinterface::ContainerFactory::setInstance(std::shared_ptr<hltinterface::ContainerFactory> ptr){
  if(m_instance){
    throw std::logic_error("Container instance is already set!");
    return false;
  }
  m_instance=ptr;
  return true;
}

namespace hltinterface{
  
  std::shared_ptr<hltinterface::GenericHLTContainer> ContainerFactory::constructContainer(const std::string &objName, //name of the object on IS
											  const std::string &objTypeName, //ISType if it is a concrete type
											  hltinterface::GenericHLTContainer::RetentionPolicy pol,
											  hltinterface::GenericHLTContainer::IOMode mode){
    if(!m_modificationEnabled){
      throw std::logic_error("Can't create new objects once event loop is started. Please use cloneContainerStructure method");
    }
    if(objName.empty()){
      throw std::invalid_argument("Object name can't be empty");
    }
    if(objTypeName.empty()){
      throw std::invalid_argument("Object type name can't be empty");
    }
    auto a = new hltinterface::GenericHLTContainer(objName,objTypeName,pol,mode);
    return std::shared_ptr<hltinterface::GenericHLTContainer>(a);
  }
  
  size_t ContainerFactory::addFloat(std::shared_ptr<hltinterface::GenericHLTContainer> &cont,
					  const std::string &fieldName){
    if(!m_modificationEnabled){
      throw std::logic_error("Can't modify object structure once event loop is started");
    }
    return cont->addFloat(fieldName);
  }

  size_t ContainerFactory::addFloatVector(std::shared_ptr<hltinterface::GenericHLTContainer> &cont,
						const std::string &fieldName,
						hltinterface::GenericHLTContainer::ContainerProperty c){
    if(!m_modificationEnabled){
      throw std::logic_error("Can't modify object structure once event loop is started");
    }
    return cont->addFloatVector(fieldName,c);
  }

  size_t ContainerFactory::addInt(std::shared_ptr<hltinterface::GenericHLTContainer> &cont,
					const std::string &fieldName){
    if(!m_modificationEnabled){
      throw std::logic_error("Can't modify object structure once event loop is started");
    }
    return cont->addInt(fieldName);
  }

  size_t ContainerFactory::addIntVector(std::shared_ptr<hltinterface::GenericHLTContainer> &cont,
					      const std::string &fieldName,
					      hltinterface::GenericHLTContainer::ContainerProperty c){
    if(!m_modificationEnabled){
      throw std::logic_error("Can't modify object structure once event loop is started");
    }
    return cont->addIntVector(fieldName,c);
  }

  size_t ContainerFactory::addString(std::shared_ptr<hltinterface::GenericHLTContainer> &cont,
					const std::string &fieldName){
    if(!m_modificationEnabled){
      throw std::logic_error("Can't modify object structure once event loop is started");
    }
    return cont->addString(fieldName);
  }

  size_t ContainerFactory::addStringVector(std::shared_ptr<hltinterface::GenericHLTContainer> &cont,
					   const std::string &fieldName){
    if(!m_modificationEnabled){
      throw std::logic_error("Can't modify object structure once event loop is started");
    }
    return cont->addStringVector(fieldName);
  }

  std::shared_ptr<hltinterface::GenericHLTContainer> ContainerFactory::cloneObject(const std::shared_ptr<hltinterface::GenericHLTContainer> &orig){
    auto n=new hltinterface::GenericHLTContainer(*orig);
    return std::shared_ptr<hltinterface::GenericHLTContainer>(n);
  }

  std::shared_ptr<hltinterface::GenericHLTContainer> ContainerFactory::cloneContainerStructure(const std::shared_ptr<hltinterface::GenericHLTContainer> &orig,const std::string &newName){
    if(newName.empty()){
      throw std::invalid_argument("Object name for the clone can't be empty");
    }
    
    auto n=new hltinterface::GenericHLTContainer(orig->getObjName(),orig->getTypeName(),orig->getPolicy(),orig->getIOMode());
    //copy int fields
    auto fnames=orig->getFieldNames(hltinterface::GenericHLTContainer::INT);
    for(auto f:fnames)n->addInt(f);
    //copy float fields
    fnames=orig->getFieldNames(hltinterface::GenericHLTContainer::FLOAT);
    for(auto f:fnames)n->addFloat(f);
    //copy string fields
    fnames=orig->getFieldNames(hltinterface::GenericHLTContainer::STRING);
    for(auto f:fnames)n->addString(f);
    //copy string vectors
    fnames=orig->getFieldNames(hltinterface::GenericHLTContainer::STRINGVEC);
    for(auto f:fnames)n->addStringVector(f);
    //copy int vectors
    fnames=orig->getFieldNames(hltinterface::GenericHLTContainer::INTVEC);
    auto fprops=orig->getFieldProperties(hltinterface::GenericHLTContainer::INTVEC);
    for(uint i=0;i<fnames.size();i++){
      n->addIntVector(fnames.at(i),fprops.at(i));
    }
    //copy float vectors
    fnames=orig->getFieldNames(hltinterface::GenericHLTContainer::FLOATVEC);
    fprops=orig->getFieldProperties(hltinterface::GenericHLTContainer::FLOATVEC);
    for(uint i=0;i<fnames.size();i++){
      n->addFloatVector(fnames.at(i),fprops.at(i));
    }

    n->setTag(orig->getTag());
    return std::shared_ptr<hltinterface::GenericHLTContainer>(n);
  }

}
