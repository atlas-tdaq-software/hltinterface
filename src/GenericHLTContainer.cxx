#include "hltinterface/GenericHLTContainer.h"
#include <stdexcept>


hltinterface::GenericHLTContainer::GenericHLTContainer(const std::string &ObjName, //name of the object on IS
					 const std::string &objType, //ISType if it is a concrete type
						       RetentionPolicy pol,//How often the object will be reset
						       IOMode mode):
  m_objName(ObjName),m_typeName(objType),m_policy(pol),m_mode(mode){ 
}

hltinterface::GenericHLTContainer::GenericHLTContainer(const hltinterface::GenericHLTContainer &rhs){
  floatNames=rhs.floatNames;
  floatVals=rhs.floatVals;
  intNames=rhs.intNames;
  intVals=rhs.intVals;
  floatVecVals=rhs.floatVecVals;
  floatVecNames=rhs.floatVecNames;
  intVecVals=rhs.intVecVals;
  intVecNames=rhs.intVecNames;
  m_objName=rhs.m_objName;
  m_typeName=rhs.m_typeName;
  m_policy=rhs.m_policy;
  m_tag=rhs.m_tag;
  m_mode=rhs.m_mode;
}

size_t hltinterface::GenericHLTContainer::addFloat(const std::string &name){
  size_t idx=floatNames.size();
  floatNames.push_back(name);
  floatVals.push_back(0.0);
  return idx;
}

size_t hltinterface::GenericHLTContainer::addFloatVector(const std::string &name,hltinterface::GenericHLTContainer::ContainerProperty c){
  size_t idx=floatVecNames.size();
  floatVecNames.push_back(std::make_pair(name,c));
  floatVecVals.push_back(std::vector<double>());
  return idx;
}

size_t hltinterface::GenericHLTContainer::addInt(const std::string &name){
  size_t idx=intNames.size();
  intNames.push_back(name);
  intVals.push_back(0l);
  return idx;
}

size_t hltinterface::GenericHLTContainer::addIntVector(const std::string &name,hltinterface::GenericHLTContainer::ContainerProperty c){
  size_t idx=intVecNames.size();
  intVecNames.push_back(std::make_pair(name,c));
  intVecVals.push_back(std::vector<long>());
  return idx;
}

size_t hltinterface::GenericHLTContainer::addString(const std::string &name){
  size_t idx=stringNames.size();
  stringNames.push_back(name);
  stringVals.push_back("");
  return idx;
}

size_t hltinterface::GenericHLTContainer::addStringVector(const std::string &name){
  size_t idx=stringVecNames.size();
  stringVecNames.push_back(name);
  stringVecVals.push_back(std::vector<std::string>());
  return idx;
}

void hltinterface::GenericHLTContainer::setFloatField(size_t idx,double value){
  floatVals.at(idx)=value;
}

void hltinterface::GenericHLTContainer::setIntField(size_t idx,long value){
  intVals.at(idx)=value;
}

void hltinterface::GenericHLTContainer::setStringField(size_t idx,const std::string &value){
  stringVals.at(idx)=value;
}

void hltinterface::GenericHLTContainer::setVecField(size_t idx,const std::vector<double> &values){
  floatVecVals.at(idx)=values;
}

void hltinterface::GenericHLTContainer::setVecField(size_t idx,const std::vector<long> &values){
  intVecVals.at(idx)=values;
}

void hltinterface::GenericHLTContainer::setVecField(size_t idx,const std::vector<std::string> &values){
  stringVecVals.at(idx)=values;
}

void hltinterface::GenericHLTContainer::appendField(size_t idx,const std::vector<double> &values){
  floatVecVals.at(idx).insert(floatVecVals.at(idx).end(),values.begin(),values.end());
}

void hltinterface::GenericHLTContainer::appendField(size_t idx,const std::vector<long> &values){
  intVecVals.at(idx).insert(intVecVals.at(idx).end(),values.begin(),values.end());
}

void hltinterface::GenericHLTContainer::appendField(size_t idx,const std::vector<std::string> &values){
  stringVecVals.at(idx).insert(stringVecVals.at(idx).end(),values.begin(),values.end());
}

double hltinterface::GenericHLTContainer::getFloatField(size_t idx)const{
  return floatVals.at(idx);
}

long hltinterface::GenericHLTContainer::getIntField(size_t idx)const {
  return intVals.at(idx);
}

std::string& hltinterface::GenericHLTContainer::getStringField(size_t idx){
  return stringVals.at(idx);
}

std::vector<double>&  hltinterface::GenericHLTContainer::getFloatVecField(size_t idx){
  return floatVecVals.at(idx);
}

std::vector<long>&  hltinterface::GenericHLTContainer::getIntVecField(size_t idx){
  return intVecVals.at(idx);
}

std::vector<std::string>&  hltinterface::GenericHLTContainer::getStringVecField(size_t idx){
  return stringVecVals.at(idx);
}

void hltinterface::GenericHLTContainer::resetValues(){
  for(size_t t=0;t<floatVals.size();t++){
    floatVals[t]=0.;
  }
  for(size_t t=0;t<intVals.size();t++){
    intVals[t]=0.;
  }

  for(size_t t=0;t<intVecVals.size();t++){
    intVecVals[t].clear();
  }
  for(size_t t=0;t<floatVecVals.size();t++){
    floatVecVals[t].clear();
  }
}

const std::string& hltinterface::GenericHLTContainer::getObjName()const {
  return m_objName;
}

const std::string& hltinterface::GenericHLTContainer::getTypeName()const{
  return m_typeName;
}

int hltinterface::GenericHLTContainer::getTag()const{
  return m_tag;
}

void hltinterface::GenericHLTContainer::setTag(int tag){
  m_tag=tag;
}

hltinterface::GenericHLTContainer::RetentionPolicy hltinterface::GenericHLTContainer::getPolicy()const{
  return m_policy;
}

hltinterface::GenericHLTContainer::IOMode hltinterface::GenericHLTContainer::getIOMode()const{
  return m_mode;
}

const std::vector<std::string> hltinterface::GenericHLTContainer::getFieldNames(hltinterface::GenericHLTContainer::DataType d) const {
  std::vector<std::string> vals;
  switch(d){
  case INT:
    return intNames;
    break;
  case FLOAT:
    return floatNames;
    break;
  case INTVEC:
    for(auto a:intVecNames){vals.push_back(a.first);}
    return vals;
    break;
  case FLOATVEC:
    for(auto a:floatVecNames){vals.push_back(a.first);}
    return vals;
    break;
  default:
    throw std::range_error("Unknown data type in getFieldNames");
    break;
  }

}

const std::vector<hltinterface::GenericHLTContainer::ContainerProperty> hltinterface::GenericHLTContainer::getFieldProperties(hltinterface::GenericHLTContainer::DataType d) const {
  std::vector<hltinterface::GenericHLTContainer::ContainerProperty> vals;
  switch(d){
  case INT:
    throw std::logic_error("INT types don't have properties");
    break;
  case FLOAT:
    throw std::logic_error("FLOAT types don't have properties");
    break;
  case INTVEC:
    for(auto a:intVecNames){vals.push_back(a.second);}
    return vals;
    break;
  case FLOATVEC:
    for(auto a:floatVecNames){vals.push_back(a.second);}
    return vals;
    break;
  default:
    throw std::range_error("Unknown data type in getFieldNames");
    break;
  }
}

size_t hltinterface::GenericHLTContainer::getFieldSize(hltinterface::GenericHLTContainer::DataType d) const{
  switch(d){
  case INT:
    return intNames.size();
    break;
  case FLOAT:
    return floatNames.size();
    break;
  case INTVEC:
    return intVecNames.size();
    break;
  case FLOATVEC:
    return floatVecNames.size();
    break;
  default:
    throw std::range_error("Unknown data type in getFieldNames");
    break;
  }

}
