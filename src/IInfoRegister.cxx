// -*- c++ -*-
#include "hltinterface/IInfoRegister.h"
#include "hltinterface/ContainerFactory.h"
#include <iostream>
#include <iomanip>

hltinterface::IInfoRegister* hltinterface::IInfoRegister::m_instance=0;
hltinterface::IInfoRegister* hltinterface::IInfoRegister::instance(){return m_instance;}

bool hltinterface::IInfoRegister::setInstance(hltinterface::IInfoRegister * i,bool force) {
  if ( m_instance){
    if(!force){
      return false;
    }else{
      std::cerr<<"Warning overwriting old IInfoRegister instance "
	       <<std::setbase(16)<<(void*)m_instance<<" "
	       <<(void*)i<<std::setbase(10)<<std::endl;
    }
  }
  m_instance = i;
  return true;
}

void hltinterface::IInfoRegister::p_setModification(bool b){
  hltinterface::ContainerFactory::getInstance()->setModification(b);
}
