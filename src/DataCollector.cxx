//Dear emacs, this is -*- c++ -*-

/**
 * @file DataCollector.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * $Author$
 * $Revision$
 * $Date$
 *
 * @brief Defines how to access event data collector, in abstract terms
 */

#include "hltinterface/DataCollector.h"

void hltinterface::DataCollector::instance(DataCollector *d) 
{
    s_instance = d;
}

hltinterface::DataCollector* hltinterface::DataCollector::instance()
{
    return s_instance;
}

hltinterface::DataCollector *hltinterface::DataCollector::s_instance = 0;

