//Dear emacs, this is -*- c++ -*-

/**
 * @file DataCollector.h
 * @brief Defines how to access event data on abstract terms.
 */

#ifndef HLTINTERFACE_DATACOLLECTOR_H
#define HLTINTERFACE_DATACOLLECTOR_H

#include "hltinterface/DCM_ROBInfo.h"
#include "eformat/ROBFragment.h"
#include "eformat/write/FullEventFragment.h"

#include <utility>
#include <vector>
#include <stdint.h>

namespace hltinterface {

  /**
   * An abstract view of a DataCollector.
   */
  class DataCollector {

  public:

    /**
     * DataCollector status code
     */
    enum class Status {
      OK = 0,      ///< event returned
      NO_EVENT,    ///< no event available
      STOP         ///< stop transition (no more events)
    };

    /**
     * Virtualizes the destruction.
     */
    virtual ~DataCollector () {}

    /**
     * @brief Request the next event.
     *
     * A FullEventFragment is filled with partial event data containing at the
     * minimum the L1 RoI ROBs and a correctly filled event header. The fragment
     * is serialised and the ownership is passed to \p l1r. The caller creates
     * the unique_ptr object \p l1r and is responsible for releasing the pointer
     * after use.
     * 
     * The following fields of the header should be correctly set to the
     * respective values for a given event:
     * version, global_id, run_type, run_no, lumi_block, bc_id, bc_time_seconds,
     * bc_time_nanoseconds, lvl1_id, lvl1_trigger_type, lvl1_trigger_info
     * 
     * In addition:
     * source_id should be FULL_SD_EVENT
     * status should be normally 0
     * compression_type should be UNCOMPRESSED
     * checksum can be NO_CHECKSUM
     * 
     * The following fields should be empty:
     * lvl2_trigger_info, event_filter_info, hlt_info, stream_tag
     *
     * @param  l1r reference to a unique_ptr which should take ownership of
     *         a serialized FullEventFragment containing LVL1 result ROBs
     * @return Status code (e.g. stop transition requested)
     */
    virtual Status getNext (std::unique_ptr<uint32_t[]>& l1r) =0;

    /**
     * Event processing finished
     *
     * Any results produced by the HLT (trigger bits, stream tags, HLT result ROBs)
     * are serialized into a FullEventFragment. Ownership is transferred to the callee.
     *
     * @param hltr  serialized FullEventFragment containing outputs of HLT processing
     */
    virtual void eventDone (std::unique_ptr<uint32_t[]> hltr) =0;

    /**
     * Reserve a series of ROB's for possible future retrieval. This method allows to
     * tell the DataCollector what ROBs may be needed based on the geometrical RoI
     * dimensions. In this way ROB requests can be grouped and the ROS access rate can
     * be reduced. The method can be called several times before an actual ROB retreval
     * happens with the "collect" method.
     * 
     * @param global_id The global event identifier for the data of interest
     * @param ids The identifiers of the ROBs to reserve.
     */
    virtual void reserveROBData(const uint64_t global_id, const std::vector<uint32_t>& ids) = 0;

    /**
     * Collects a series of ROB's, given their identifiers. This method should
     * return in addition to the number of ROB's successfuly collected also their
     * corresponding ROBInfo objects with additional information for cost monitoring
     * 
     * @return data returned vector of ROBInfo objects
     * @param  global_id The global event identifier for the data of interest
     * @param  ids The identifiers of each ROB requested.
     */
    virtual uint32_t collect(std::vector<hltinterface::DCM_ROBInfo>& data,
                             const uint64_t global_id, 
                             const std::vector<uint32_t>& ids) = 0;

    /**
     * Collect all remaining data for this event, aka event building. This method should
     * return in addition to the number of ROB's successfuly collected also their
     * corresponding ROBInfo objects with additional information for cost monitoring.
     *
     * @return data returned vector of ROBInfo objects 
     * @param  global_id The global event identifier for the data of interest
     */
    virtual uint32_t collect(std::vector<hltinterface::DCM_ROBInfo>& data,
                             const uint64_t global_id) = 0;

  public:

    /**
     * Sets the DataCollector instance
     */
    static void instance (DataCollector* d);

    /**
     * Gets the DataCollector instance
     */
    static DataCollector* instance (void);

  private:

    static DataCollector *s_instance; ///< my instance

  };

}

#endif /* HLTINTERFACE_DATACOLLECTOR_H */
