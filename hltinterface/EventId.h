//-*- c++ -*-
#ifndef HLTINTERFACE_EVENTID_H
#define HLTINTERFACE_EVENTID_H
namespace hltinterface{
  /** Event Id structure to pass global event id, 
   *  level1 id and luminosity block number to HLT for
   *  cross-checking and error reporting.
   */
  struct [[deprecated]] EventId{
    uint64_t globalId;
    uint64_t l1Id;
    uint64_t lbNumber;
  };
}
#endif
