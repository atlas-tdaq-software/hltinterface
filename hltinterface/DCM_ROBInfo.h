//Dear emacs, this is -*- c++ -*-

/**
 * @file DCM_ROBInfo.h
 * $Author: wiedenat $
 * $Date: 2013-05-02 $
 *
 * @brief Defines a structure with ROB information from the DCM.
 */

#ifndef HLTINTERFACE_DCM_ROBINFO_H
#define HLTINTERFACE_DCM_ROBINFO_H

#include "eformat/ROBFragment.h"
#include <stdint.h>
#include <chrono>

namespace hltinterface {
  /**
   * Structure which holds a ROB fragment and additinal ROB status information for
   * cost monitoring  
   */
  struct DCM_ROBInfo {
    eformat::ROBFragment<const uint32_t*>              robFragment;      // ROB fragment
    bool                                               robIsCached;      // = true if ROB came from DCM cache 
    std::chrono::time_point<std::chrono::steady_clock> robRequestTime;   // start ROB request time
    std::chrono::time_point<std::chrono::steady_clock> robDeliveryTime;  // stop  ROB request time   

    DCM_ROBInfo():
      robIsCached(false){};

    DCM_ROBInfo(const eformat::ROBFragment<const uint32_t*>& f, 
		bool c, 
		const std::chrono::time_point<std::chrono::steady_clock>& ts,
		const std::chrono::time_point<std::chrono::steady_clock>& te):
      robFragment(f), robIsCached(c), robRequestTime(ts), robDeliveryTime(te){};
  };
}
#endif /* HLTINTERFACE_DCM_ROBINFO_H */
