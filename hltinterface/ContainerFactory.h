// -*- c++ -*-
#ifndef __HLTINTERFACE__CONTAINERFACTORY_H
#define  __HLTINTERFACE__CONTAINERFACTORY_H
#include "hltinterface/GenericHLTContainer.h"
#include <memory>
#include <stdexcept>
namespace hltinterface{
  class IInfoRegister;
  class ContainerFactory{
    friend class IInfoRegister;
  public:
    ContainerFactory(){};
    ~ContainerFactory(){};
    virtual std::shared_ptr<hltinterface::GenericHLTContainer> constructContainer(const std::string &ObjName, //name of the object on IS
    							   const std::string &objTypeName, //ISType if it is a concrete type
										  hltinterface::GenericHLTContainer::RetentionPolicy pol=hltinterface::GenericHLTContainer::UntilEOR,
										  hltinterface::GenericHLTContainer::IOMode mode=hltinterface::GenericHLTContainer::WRITE);
    virtual size_t addFloat(std::shared_ptr<hltinterface::GenericHLTContainer> &cont,
    		   const std::string &fieldName); 
    virtual size_t addFloatVector(std::shared_ptr<hltinterface::GenericHLTContainer> &cont,
    			 const std::string &fieldName,
			  hltinterface::GenericHLTContainer::ContainerProperty c=hltinterface::GenericHLTContainer::LASTVALUE); 
    virtual size_t addString(std::shared_ptr<hltinterface::GenericHLTContainer> &cont,
    		   const std::string &fieldName); 
    virtual size_t addStringVector(std::shared_ptr<hltinterface::GenericHLTContainer> &cont,
				   const std::string &fieldName);
    virtual size_t addInt(std::shared_ptr<hltinterface::GenericHLTContainer> &cont,
    		 const std::string &fieldName); 
    virtual size_t addIntVector(std::shared_ptr<hltinterface::GenericHLTContainer> &cont,
				const std::string &fieldName,
				hltinterface::GenericHLTContainer::ContainerProperty c=hltinterface::GenericHLTContainer::LASTVALUE);
    virtual std::shared_ptr<hltinterface::GenericHLTContainer> cloneContainerStructure(const std::shared_ptr<hltinterface::GenericHLTContainer> &orig,const std::string &newName);
    virtual std::shared_ptr<hltinterface::GenericHLTContainer> cloneObject(const std::shared_ptr<hltinterface::GenericHLTContainer> &orig);

    static std::shared_ptr<hltinterface::ContainerFactory> getInstance();
    static bool setInstance(std::shared_ptr<hltinterface::ContainerFactory> ptr);
  protected:
    void setModification(bool m){m_modificationEnabled=m;};
    static bool m_modificationEnabled;
  private:
    //operator delete(void *){};//make it private and create a deleter friend to prevent accidental deletion
    static std::shared_ptr<hltinterface::ContainerFactory> m_instance;

  };
}
#endif
