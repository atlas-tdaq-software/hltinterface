//Dear emacs, this is -*- c++ -*-

#ifndef HLTINTERFACE_HLTRESULT_H
#define HLTINTERFACE_HLTRESULT_H

#include "eformat/StreamTag.h"
#include "eformat/ROBFragmentNoTemplates.h"
#include <vector>
#include <stdint.h>

namespace hltinterface {

  /**
   * The HLT result structure which is exchanged between the HLTPU and the PSC
   * It holds all the decision data produced by the HLT algorithms
   */
  struct [[deprecated]] HLTResult {
    /** 
     * Default maximum number of words in HLT result ROB buffer 
     *
     * The value is in 4 byte words (32 bits).
     * Example: 10Mb = 1024 * 1024 * 10 bytes
     *               = 1024 * 1024 * 10/4 words-of-4-bytes
     */
    static const size_t DEFAULT_MAX_RESULTSIZE = 1024 * 1024 * 10 / 4;

    std::vector<uint32_t> trigger_info;                     // HLT trigger info words
    std::vector<eformat::helper::StreamTag> stream_tag;     // stream tags with list of ROBs/SDs for partial event building
    std::vector<uint32_t> psc_errors;                       // vector of PSC error words
    /**
     * The HLT result ROB fragments are organized in the hltResult_robs vector. Each object in this vector internally refers to
     * a piece of memory that contains the serialized version of a ROB fragment. Furthermore, this memory is part of the buffer 
     * that starts at fragment_pointer and spans max_result_size 4 byte words. The buffer is normally allocated, managed and 
     * owned by the HLTMPPU, which guarantees that its lifetime is long enough so that the ROB fragments can be copied to the DCM.
     */
    std::vector<eformat::read::ROBFragment> hltResult_robs;   // vector of HLT result ROB fragments (standard HLT result + Data Scouting ROBs)
    uint32_t                                max_result_size;  // the maximum buffer size one can use for all HLT result ROBs
    uint32_t*                               fragment_pointer; // Pointer to the buffer where the HLT result ROB fragments are stored.
   };
}
#endif /* HLTINTERFACE_HLTRESULT_H */
