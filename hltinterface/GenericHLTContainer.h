// -*- c++ -*-
#ifndef HLTINTERFACE_GENERICHLTCONTAINER_H
#define HLTINTERFACE_GENERICHLTCONTAINER_H
//#pragma once

#include <string>
#include <vector>
#include <utility>
namespace hltinterface{
  class ContainerFactory;
  class GenericHLTContainer{
  public:
    enum IOMode{WRITE=1,//value is written to sink as per publication configuration
		READ_PULL=2, //value is read from source only on demand no publication
		UPDATE_PULL=4,//value is read from source only on demand, written as usual
		READ_CALLBACK=16, //value is updated via a callback from source (IS)
		UPDATE_CALLBACK=32// value is updated via a callback, written as usual
    };
    enum RetentionPolicy{ UntilEOR=1, PerLB=2, PerPublication=4 };
    enum DataType{INT=1, FLOAT=2,INTVEC=3,FLOATVEC=4,STRING=8,STRINGVEC=16};
    enum ContainerProperty{LASTVALUE=0,//user will keep track and always last value will be used
			   ACCUMULATOR=1 //Container will keep track and
			   //will try to keep differences
			   //between last publish and most
			   //recent in the object for perPublish or PerLB objects.
    };
  private:
    std::vector< std::string > floatNames;
    std::vector<double> floatVals;
    std::vector< std::string > intNames;
    std::vector<long> intVals;
    std::vector<std::vector<double> > floatVecVals;
    std::vector<std::pair<std::string,ContainerProperty> > floatVecNames;
    std::vector<std::vector<long> > intVecVals;
    std::vector<std::pair<std::string,ContainerProperty> > intVecNames;
    std::vector<std::string> stringNames,stringVals,stringVecNames;
    std::vector<std::vector<std::string> > stringVecVals;
    std::string m_objName;
    std::string m_typeName;
    RetentionPolicy m_policy;
    IOMode m_mode;
    int m_tag;
  protected:
    friend class hltinterface::ContainerFactory;
    GenericHLTContainer(const std::string& ObjName, //name of the object on IS
			const std::string& objType, //ISType if it is a concrete type
			RetentionPolicy pol=UntilEOR,//How often the object will be reset
			IOMode mode=WRITE); // what
    GenericHLTContainer(const GenericHLTContainer&);
    size_t addFloat(const std::string &name);
    size_t addFloatVector(const std::string &name,GenericHLTContainer::ContainerProperty c=LASTVALUE);
    size_t addInt(const std::string &name);
    size_t addIntVector(const std::string &name,GenericHLTContainer::ContainerProperty c=LASTVALUE);
    size_t addString(const std::string &name);
    size_t addStringVector(const std::string &name);
  public:
    void   setFloatField(size_t idx,double value);
    void   setIntField(size_t idx,long value);
    void   setStringField(size_t idx,const std::string &value);
    void   setVecField(size_t idx,const std::vector<double> &values);
    void   setVecField(size_t idx,const std::vector<long> &values);
    void   setVecField(size_t idx,const std::vector<std::string> &values);
    void   appendField(size_t idx,const std::vector<double> &values);
    void   appendField(size_t idx,const std::vector<long> &values);
    void   appendField(size_t idx,const std::vector<std::string> &values);
    double getFloatField(size_t idx) const;
    long   getIntField(size_t idx) const;
    std::string& getStringField(size_t idx);
    std::vector<double>&  getFloatVecField(size_t idx);
    std::vector<long>&  getIntVecField(size_t idx);
    std::vector<std::string>& getStringVecField(size_t idx);
    void resetValues();
    const std::string& getObjName() const;
    const std::string& getTypeName() const;
    int getTag() const ;
    void setTag(int tag);
    size_t getFieldSize(DataType d) const;
    const std::vector<std::string> getFieldNames(GenericHLTContainer::DataType d) const;
    const std::vector<GenericHLTContainer::ContainerProperty> getFieldProperties(GenericHLTContainer::DataType d) const;
    GenericHLTContainer::RetentionPolicy getPolicy() const;
    GenericHLTContainer::IOMode getIOMode() const;
  };
}
#endif
