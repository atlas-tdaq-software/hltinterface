// -*- c++ -*-
/**
 *  Merged IISRegister and ITHistRegister interface
 *  S. Kama
 */
#ifndef HLTINTERFACE_IINFOREGISTER_H
#define HLTINTERFACE_IINFOREGISTER_H
#include <memory>
#include <map>
#include <string>
#include <vector>
#include <mutex>
#include <boost/property_tree/ptree.hpp>

class TObject;

namespace hltinterface{
  class GenericHLTContainer;
  class IInfoRegister{
  protected:
    IInfoRegister(){};
  public:
    typedef std::map<std::string, TObject*> THList;
    virtual ~IInfoRegister(){};

    virtual bool prepareForRun(const boost::property_tree::ptree &args)=0;
    virtual bool prepareWorker(const boost::property_tree::ptree &args)=0;
    virtual bool finalizeWorker(const boost::property_tree::ptree &args)=0;
    /**
     * @brief the only accessor method
     */
    static IInfoRegister* instance();
    virtual std::vector<std::shared_ptr<GenericHLTContainer> > queryISRegistry(const std::string& regex)=0;
    /** 
     * @brief this is to set actual implementation
     */
    static bool setInstance(IInfoRegister* inst,bool force=false);
    //methods from IISregister
    virtual bool configure(const boost::property_tree::ptree &args)=0; //in ISRegister
    virtual bool finalize(const boost::property_tree::ptree &args)=0;// in ISRegister
    /**
     * @brief Method for registering object for IS publication
     */
    virtual bool registerObject(const std::string publishPath, std::shared_ptr<GenericHLTContainer> object)=0;
    /**
     * @brief Method for de-registering object for IS publication
     */
    virtual bool releaseObject(const std::string publishPath)=0;
    /**
     * \brief Method to pull info from source (e.g. IS) into the named object, updating its contents
     * with the values in the source
     */
    virtual bool pullInfo(const std::string &publishPath,const std::string &objectName)=0;
    /**
     * @brief Method for marking start of event processing for IS publication
     */
    virtual bool beginEvent(const boost::property_tree::ptree &)=0;
    /**
     * @brief Method for marking end of event processing for IS publication
     */
    virtual bool endEvent(const boost::property_tree::ptree &)=0;

    // methods from ITHistRegister, some of them are probably unnecessary;
    /**
     * @brief method to get list of histograms
     * which start from certain name
     * @param regex to match booking path
     * @param list the list of type THList of all histograms matching
     */
    virtual void get(const std::string& regex, THList& list) = 0;
    /**
     * @brief method to get all histograms pointers without internal summing
     * @param regex to match booking path
     * @param list the map from booking path to all summed up under that name in vector
     */
    virtual void getUnsummed(const std::string& regex, std::map<std::string, std::vector<TObject*> >& list) = 0;
    /**
     * @brief method to clear the register (and possibly delete histograms)
     * @param regex to match booking path
     */
    virtual void clear(const std::string& regex ) = 0;
    /**
     * @brief method to reset histograms 
     * @param regex to match booking path
     */
    virtual void reset(const std::string& regex ) = 0;
    /**
     * @brief method for registration of the histograms
     * @param sname is service name (see getTObject)
     * @param id (unique ID) 
     * @param h ROOT histogram pointer
     * @return true if success false otherwise
     * If more than one histogram is tried to be registered 
     * under the same ID then the compatibility is checked.
     * The "number of bins" and "name" should be identical.
     */
    virtual bool registerTObject (const std::string& sname, const std::string& id, 
    				  TObject* h) = 0;
    /** 
     * @brief method to get registered histograms (i.e. in another Algorithm)
     * This method should be used by the services instead of i.e. get
     * @param sname service name 
     *        (or any name which allows to get later on certain histogram
     * becaues sname+id is unique. In offline framework this is service name.
     * @param id unique ID 
     * @param h place to put the ROOT histogram pointer
     * @return false if failed (also the pointer is set to 0)
     */
    virtual bool discoverTObject (const std::string& sname, const std::string& id,
    				  TObject *&h  ) = 0;
    /** 
     * @brief method to signal that histogram will not be used any more
     * @param sname service name 
     *        (or any name which allows to get later on certain histogram
     * becaues sname+id is unique. In offline framework this is service name.
     * @param id unique ID 
     */
    virtual bool releaseTObject (const std::string& sname, 
    				 const std::string& id ) = 0;
    /**
     * \brief Clear the histograms that are pending release and that
     *        match regex.
     * @param regex The regular expression that specifies which of the 
     *  histograms signaled for release should be actually released now
     * @see releaseTObject
     */
    virtual void clearToRelease(const std::string& regex) = 0;

    using mutex_type = std::mutex;
    /**
     * \brief get a handle to the mutex for locking histogram operations
     */
    virtual mutex_type& getPublicationMutex() const = 0;
    /**
     * \brief Method to block registration of new objects
     */
    void virtual setModification(bool b)=0;

  protected:
    void p_setModification(bool b);
    static IInfoRegister* m_instance;
  };

}
#endif
