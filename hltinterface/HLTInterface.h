//Dear emacs, this is -*- c++ -*-
#ifndef HLTINTERFACE_HLTINTERFACE_H
#define HLTINTERFACE_HLTINTERFACE_H

/**
 * @brief Interface definition between HLT and DF.
 */

#include <boost/property_tree/ptree.hpp>
#include "eformat/ROBFragment.h"
#include <memory>
#include <vector>
#include <stdint.h>

namespace hltinterface {

  /**
   * Defines common operations for the combined HLT software.
   */
  class HLTInterface {

  public:

    /**
     * Virtualizes the destruction.
     */
    virtual ~HLTInterface () {}
      
    /**
     * Configures the framework
     *
     * @param config Is the only configuration parameter passed. The
     * actual configuration implementation has to parse it, if that is the
     * case in order to find its own way through the framework's configuration
     * system.
     */
    virtual bool configure (const boost::property_tree::ptree& config) =0;

    /**
     * Connects the framework
     */
    virtual bool connect (const boost::property_tree::ptree& args) =0;

    /**
     * prepares the HLT framework for a run
     *
     * @param run_number The Run number to be used for this run.
     */
    virtual bool prepareForRun (const boost::property_tree::ptree& args) =0;

    /**
     * stops the HLT framework without re-configuring
     */
    virtual bool stopRun (const boost::property_tree::ptree& args) =0;

    /**
     * Disconnects the framework
     */
    virtual bool disconnect (const boost::property_tree::ptree& args) =0;

    /**
     * Unconfigures the framework, releasing all acquired resources.
     */
    virtual bool unconfigure (const boost::property_tree::ptree& args) =0;

    /**
     * Calls the HLT framework to publish statistics, after the run has
     * finished. 
     */
    virtual bool publishStatistics (const boost::property_tree::ptree& args) =0;

    /**
     * Calls the HLT framework to notify it that a user command has arrived
     *
     * @param usrCmd   The user command name 
     * @param usrParam Vector of string parameters from user command
     */
    virtual bool hltUserCommand (const boost::property_tree::ptree& args) =0;

    /**
     * Start the event loop
     *
     * The HLT will start requesting events only after this has been called.
     *
     * @return true if all events were processed successfully, false if the
     *         processing fails
     */
    virtual bool doEventLoop () =0;

    /**
     * Method which can be called for a worker to perform the necessary steps to set
     * unique worker IDs and adapt histogram paths 
     */
    virtual bool prepareWorker (const boost::property_tree::ptree& args) =0;

    /**
     * Method which can be called for a worker to perform a cleanup before the
     * worker gets killed 
     */
    virtual bool finalizeWorker (const boost::property_tree::ptree& args) =0;
  };

}
#endif /* HLTINTERFACE_HLTINTERFACE_H */
